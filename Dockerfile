FROM node:7.2.0

EXPOSE 3000

ARG PWD
# ENTRYPOINT &{["elm"]}

WORKDIR /code
COPY . .

RUN npm cache clean -f
RUN npm install
RUN npm install -g foreman
# RUN npm install -g elm@0.18.0
# RUN npm install -g elm-test@0.18.0

# need install npm for
# RUN apt-get -y update && apt-get -y install npm
# install and run elm-webpack-loader
# https://github.com/rtfeldman/elm-webpack-loader
# RUN npm install --save elm-webpack-loader
# BUT i don't known if it really necessary...
# https://maximilianhoffmann.com/posts/how-to-compile-elm-files-on-save
# RUN elm-package install -y

