#!/bin/bash
. ./variable_config_env.sh

# not needed during development
# docker run --name ${app} -v $(pwd):/code --rm -it "${group}/${app}" make --yes src/Main.elm --output=index.html

# deprecated using elm-reactor server
# docker run -p 8000:8000 --name ${app} -v $(pwd):/code --rm -it "${group}/${app}" elm-reactor -a 0.0.0.0

# use instead of elm-reactor server, webpack dev server launch by foreman
docker run -p 3000:3000 --name ${app} -v $(pwd):/code --rm -it "${group}/${app}" nf start