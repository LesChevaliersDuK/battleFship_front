module Model exposing (model)

import Material
import Types exposing (..)

-- MODEL
model : Types.Model
model =
    { count = 0
    , mdl = Material.model
    }