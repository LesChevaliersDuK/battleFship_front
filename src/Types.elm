module Types exposing (..)

import Material

type alias Model =
    { count : Int
    , mdl : Material.Model
    }

type alias Ship =
    { id : String
    , name : String
    , choices : List String
    }

type Msg
    = Increase
    | Reset
    | Mdl (Material.Msg Msg)
    
type alias Mdl =
    Material.Model

