module Update exposing (update)

import Material
import Types exposing (..)
-- ACTION, UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Increase ->
            ( { model | count = model.count + 1 }
            , Cmd.none
            )

        Reset ->
            ( { model | count = 0 }
            , Cmd.none
            )
            
        Mdl msg_ ->
            Material.update Mdl msg_ model