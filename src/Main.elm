module Main exposing (..)

import Html exposing (Html, div, text, program)
import Types exposing (..)
import Model exposing (model)
import View exposing (view)
import Update exposing (update)

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none

-- MAIN

main : Program Never Model Msg
main =
    Html.program
        { init = ( model, Cmd.none )
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }