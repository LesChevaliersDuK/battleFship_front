module View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (class, href, src)
import Array exposing (Array)
import Types exposing (..)
import Material.Scheme
import Material.Button as Button
import Material.Grid exposing (..)
import Material.Footer as Footer
import Material.Options as Options exposing (Style, css)
import Material.Color as Color

view : Model -> Html Msg
view model =
    let
        counter =
                model
                |> view1
        gameGrid =
                model
                |> view2
    in
        div
            [ class "main" ]
            [ pageHeader
            , counter
            , gameGrid
            , pageFooter
            ]
            |> Material.Scheme.top

view1 : Model -> Html Msg
view1 model = 
    div 
        [class "action_button"]
        [ div
             [Html.Attributes.style [ ( "padding", "10px" ) ]]
             [ Button.render Mdl
                [ 0 ]
                model.mdl
                [ Button.ripple
                , Button.colored
                , Button.raised
                , Options.onClick Increase
                , css "margin" "0 25px"
                ]
                [ text "Start" ]
            , Button.render Mdl
                [ 1 ]
                model.mdl
                [ Button.ripple
                , Button.colored
                , Button.raised 
                , Options.onClick Reset 
                ]
                [ text "Quit" ]
            ]
        ]
view2 : Model -> Html Msg
view2 model = 
    p
    []
    [
    List.range 1 100
    |> List.map (\i -> std [offset All 0, size All 0, color 70] [text <| toString model.count])
    |> grid [noSpacing, maxWidth "810px"]
    ]
        

-- LAYOUT (private)

pageHeader : Html Msg
pageHeader =
    header
        []
        [ h1
            [ class "header__title" ]
            [ text "Navy Battle Game" ]
        ]

pageFooter : Html Msg
pageFooter =
    div
        []
        [ div
            []
            -- [ a
            --     [ href "https://gitlab.com/LesChevaliersDuK/battleFship_front" ]
            --     [ text "LesChevaliersDuK on gitlab" ]
            -- ]
            [ Footer.mini []
                { left =
                    Footer.left []
                    [ Footer.logo [] [ Footer.html <| text "LesChevaliersDuK" ]
                    ]
                , right =
            Footer.right []
              [ Footer.logo [] [ Footer.html <| text "" ]
              ]
                }
            ]
        ]

-- Cell styling


style : Int -> List (Style a)
style h = 
  [ css "text-sizing" "border-box"
  , css "height" (toString h ++ "px")
  , css "width" (toString h ++ "px")
  , css "margin" "1px"
  , css "color" "none"
  ]

-- Cell


gameCell : Int -> List (Style a) -> List (Html a) -> Cell a
gameCell k styling = 
    cell <| List.concat [style k, styling] 

std : List (Style a) -> List (Html a) ->Cell a
std = gameCell 79

-- Grid 


color : Int -> Style a
color k =
    Array.get ((k + 0) % Array.length Color.hues) Color.hues
      |> Maybe.withDefault Color.Teal
      |> flip Color.color Color.S500
      |> Color.background