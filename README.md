# BattleFShip Front

Pretty simple app for play navy battle game, written in Elm.

## Setup

Prerequesite is Docker [https://www.docker.com/products/docker#/linux].

## Building and running the app

```bash
$ ./build.sh && ./run.sh
```

This builds the app with webpack and gives you a live-reloading server on [`0.0.0.0:3000`](http://0.0.0.0:3000)

## Used tools

- [elm-webpack-loader](https://github.com/rtfeldman/elm-webpack-loader)

## Acknowledgements
